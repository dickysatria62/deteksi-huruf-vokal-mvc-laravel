<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Hitung Vokal</title>
    <style>
      .container {
        max-width: 900px;
        padding: 0 20px;
        margin: 0 auto;
      }
      .form {
        align-items: center;
      }
      .heading2 {
        padding-top: 20px;
        padding-bottom: 10px;
        text-align: center;
      }

      input {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
      }

      .submit {
        width: 100%;
        background-color: #3273c8;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        border-radius: 4px;
        cursor: pointer;
      }

      .submit:hover {
        background-color: #4d8fd2;
      }

      h3 {
        text-align: center;
      }
    </style>
  </head>
  <body>
      @yield('content')
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
  </body>
</html>
