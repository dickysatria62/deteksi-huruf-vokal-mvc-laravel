@extends('layouts.head')
@section('content')

    <div class="container mt-5">
        <h1 class="display-4">Hasil</h1>
        @if ($jumlah > 0)
            <span>"{{ $kata }}" = {{ $jumlah }} yaitu @foreach ($vokals as $item)
                    {{ $item }}
                @endforeach
            </span>
        @endif
    </div>

